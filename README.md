# Learning Go

Just a project to put all of the things Golang, while I am trying to learn the basics.

## Online Courses
https://www.udemy.com/course/go-the-complete-developers-guide/

## Go gotcha
Go is pass by value. **But** some types like slices, contain a
pointer to the underlying array. So doing the following, will update
the "value" within the passed in slice. Where the same behaviour will
not be seen with a struct.
```
func updateSlice(s []string) {
	s[0] = "Bye"
}
```