package main

import "fmt"

func main() {
	cards := newDeck()

	hand, remainingDeck := deal(cards, 5)

	fmt.Println("-- Full deck")
	fmt.Println(cards.toString())

	fmt.Println("-- Deal a hand")
	hand.print()
	remainingDeck.print()

	fmt.Println("-- Saving to HDD")
	cards.saveToFile("my_cards")

	fmt.Println("-- Creating deck from file")
	newDeck := newDeckFromFile("my_cards")
	newDeck.print()

	fmt.Println("-- Shuffle cards")
	cards.shuffle()
	cards.print()
}
