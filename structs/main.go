package main

import "fmt"

type contactInfo struct {
	email   string
	zipCode int
}

type person struct {
	firstName string
	lastName  string
	contact   contactInfo
}

func main() {
	jim := person{
		firstName: "Jim",
		lastName:  "Party",
		contact: contactInfo{
			email:   "test@info.com",
			zipCode: 94000,
		},
	}
	jim.updateName("Jimmy")
	jim.print()
}

func (p person) print() {
	fmt.Printf("%+v\n", p)
}

func (p *person) updateName(newFirstName string) {
	(*p).firstName = newFirstName
}

/*
Go pointer notes:
	tldr
		Address into value -> *address
		Value into address -> &value
	jimPointer := &jim
		`&` gets a reference to the address in memory,
		 of person the value
	func (p *person)
		`*` in front of type, means we are looking for pointer to type
	(*p)
		Here `*` is an operator that turns it into the value referenced
		by the pointer.
*/
